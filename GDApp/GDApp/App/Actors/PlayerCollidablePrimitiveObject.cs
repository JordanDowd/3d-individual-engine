﻿using GDApp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GDLibrary
{
    public class PlayerCollidablePrimitiveObject : CollidablePrimitiveObject
    {
        #region Fields
        private float moveSpeed, rotationSpeed;
        private Keys[] moveKeys;
        private bool bThirdPersonZoneEventSent;
        private ManagerParameters managerParameters;
        private ObjectManager objectManager;
        private bool played = true;
        #endregion

        #region Properties
        #endregion

        public PlayerCollidablePrimitiveObject(string id, ActorType actorType, Transform3D transform, EffectParameters effectParameters,
            StatusType statusType, IVertexData vertexData, ICollisionPrimitive collisionPrimitive, 
            ManagerParameters managerParameters,
            Keys[] moveKeys, float moveSpeed, float rotationSpeed, ObjectManager objectManager) 
            : base(id, actorType, transform, effectParameters, statusType, vertexData, collisionPrimitive, managerParameters.ObjectManager)
        {
            this.moveKeys = moveKeys;
            this.moveSpeed = moveSpeed;
            this.rotationSpeed = rotationSpeed;

            //for input
            this.managerParameters = managerParameters;
            this.objectManager = objectManager;
        }
        
        //used to make a player collidable primitives from an existing PrimitiveObject (i.e. the type returned by the PrimitiveFactory
        public PlayerCollidablePrimitiveObject(PrimitiveObject primitiveObject, ICollisionPrimitive collisionPrimitive,
                                ManagerParameters managerParameters, Keys[] moveKeys, float moveSpeed, float rotationSpeed, ObjectManager objectManager)
            : base(primitiveObject, collisionPrimitive, managerParameters.ObjectManager)
        {
            this.moveKeys = moveKeys;
            this.moveSpeed = moveSpeed;
            this.rotationSpeed = rotationSpeed;

            //for input
            this.managerParameters = managerParameters;
            this.objectManager = objectManager;
        }


        public override void Update(GameTime gameTime)
        {
            //read any input and store suggested increments
            HandleInput(gameTime);

            //have we collided with something?
            this.Collidee = CheckCollisions(gameTime);

            //how do we respond to this collidee e.g. pickup?
            HandleCollisionResponse(this.Collidee);

            //if no collision then move - see how we set this.Collidee to null in HandleCollisionResponse() 
            //below when we hit against a zone
            if (this.Collidee == null)
                ApplyInput(gameTime);

            HandleGravity(gameTime);

            //reset translate and rotate and update primitive
            base.Update(gameTime);
        }

        protected override void HandleGravity(GameTime gameTime)
        {
        }

        //this is where you write the application specific CDCR response for your game
        protected override void HandleCollisionResponse(Actor collidee)
        {
            if(collidee is SimpleZoneObject)
            {
                if (collidee.ID.Equals(AppData.SwitchToThirdPersonZoneID))
                {
                    if (!bThirdPersonZoneEventSent) //add a boolean to stop the event being sent multiple times!
                    {
                        //publish some sort of event - maybe an event to switch the camera?
                        object[] additionalParameters = { AppData.ThirdPersonCameraID };
                        EventDispatcher.Publish(new EventData(EventActionType.OnCameraSetActive, EventCategoryType.Camera, additionalParameters));
                        bThirdPersonZoneEventSent = true;
                    }
                    //setting this to null means that the ApplyInput() method will get called and the player can move through the zone.
                    this.Collidee = null;
                }

                else if (collidee.ID.Equals(AppData.RemoveBarrierZoneID))
                {
                    if (played)
                    {
                        object[] additionalParametersA = { "breakingBox" };
                        EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParametersA));
                        played = false;
                    }
                    
                    foreach (IActor actor in this.objectManager.OpaqueDrawList)
                    {
                        collidee = remove(actor as Actor3D);
                    }

                    foreach (IActor actor in this.objectManager.TransparentDrawList)
                    {
                        collidee = remove(actor as Actor3D);
                    }
                    //setting this to null means that the ApplyInput() method will get called and the player can move through the zone.
                    this.Collidee = null;
                }
                else if (collidee.ID.Equals(AppData.ActivateLevelTwo))
                {
                    played = true;
                    if (played)
                    {
                        object[] additionalParametersA = { "coinPickUp" };
                        EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParametersA));
                        played = false;
                    }
                    //setting this to null means that the ApplyInput() method will get called and the player can move through the zone.
                    this.Collidee = null;
                }
            }
            else if(collidee is CollidablePrimitiveObject)
            {
                if (collidee.ActorType == ActorType.CollidableActivatable)
                {
                    //we dont HAVE to do anything here but lets change its color just to see something happen
                    //(collidee as DrawnActor3D).EffectParameters.DiffuseColor = Color.Blue;
                    object[] additionalParametersA = { "death" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParametersA));
                    //EventDispatcher.Publish(new EventData(, EventActionType.OnLose, EventCategoryType.PlayerLose));
                    this.Transform = new Transform3D(new Vector3(2, 0, 2), Vector3.Zero, new Vector3(0.99f, 0.99f, 0.99f), -Vector3.UnitZ, Vector3.UnitY); 
                }

                //decide what to do with the thing you've collided with
                else if (collidee.ActorType == ActorType.CollidableCoin)
                {
                    //do stuff...maybe a remove
                    EventDispatcher.Publish(new EventData(collidee, EventActionType.OnRemoveActor, EventCategoryType.SystemRemove));
                    object[] additionalParametersA = { "coinPickUp" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParametersA));
                    this.Collidee = null;
                }

                //activate some/all of the controllers when we touch the object
                else if (collidee.ActorType == ActorType.CollidableActivatable)
                {
                    //when we touch get a particular controller to start
                   // collidee.SetAllControllers(PlayStatusType.Play, x => x.GetControllerType().Equals(ControllerType.SineColorLerp));

                    //when we touch get a particular controller to start
                    collidee.SetAllControllers(PlayStatusType.Play, x => x.GetControllerType().Equals(ControllerType.PickupDisappear));
                }
            }
        }

        private Actor remove(Actor3D actor3D)
        {
            //dont test for collision against yourself - remember the player is in the object manager list too!
            if (this != actor3D)
            {
                if (actor3D is CollidablePrimitiveObject)
                {
                    if (actor3D.ActorType == ActorType.CollidableDestoryable)
                    {
                        //we dont HAVE to do anything here but lets change its color just to see something happen
                        //(actor3D as DrawnActor3D).EffectParameters.DiffuseColor = Color.Blue;
                        EventDispatcher.Publish(new EventData(actor3D, EventActionType.OnRemoveActor, EventCategoryType.SystemRemove));
                    }
                }
            }
            return null;
        }

        protected override void HandleInput(GameTime gameTime)
        {
            if (this.managerParameters.KeyboardManager.IsFirstKeyPress(this.moveKeys[AppData.IndexMoveForward])) //Forward
            {
                object[] additionalParametersA = { "walk" };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParametersA));
                this.Transform.TranslateIncrement
                    = new Vector3(0, 0, -1);
            }
            else if (this.managerParameters.KeyboardManager.IsFirstKeyPress(this.moveKeys[AppData.IndexMoveBackward])) //Backward
            {
                object[] additionalParametersA = { "walk" };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParametersA));
                this.Transform.TranslateIncrement
                    = new Vector3(0, 0, 1);
            }

            if (this.managerParameters.KeyboardManager.IsFirstKeyPress(this.moveKeys[AppData.IndexRotateLeft])) //Left
            {
                object[] additionalParametersA = { "walk" };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParametersA));
                //this.Transform.RotateIncrement = gameTime.ElapsedGameTime.Milliseconds * this.rotationSpeed;
                this.Transform.TranslateIncrement
                    = new Vector3(-1, 0, 0);
            }
            else if (this.managerParameters.KeyboardManager.IsFirstKeyPress(this.moveKeys[AppData.IndexRotateRight])) //Right
            {
                object[] additionalParametersA = { "walk" };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParametersA));
                //this.Transform.RotateIncrement = -gameTime.ElapsedGameTime.Milliseconds * this.rotationSpeed;
                this.Transform.TranslateIncrement
                    = new Vector3(1, 0, 0);
            }
        }
    }
}
